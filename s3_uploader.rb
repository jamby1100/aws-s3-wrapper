require 'aws-sdk'

class Uploader
  def initialize(options)
    @from = options[:from_file]
    @to = options[:to_file]
    @bucket = options[:bucket]
    @region = options[:region]
    @akid = options[:akid]
    @secret_key = options[:secret_key]
  end

  def upload()
    s3 = Aws::S3::Resource.new(
      credentials: Aws::Credentials.new(@akid, @secret_key), 
      region: @region 
      )
    object = s3.bucket(@bucket).object(@to)
    object.upload_file(@from, acl:'public-read')
    print "Successfully uploaded to this url: " + object.public_url.to_s
  end
end