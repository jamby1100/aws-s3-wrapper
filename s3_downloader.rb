require 'aws-sdk'
class Downloader
  def initialize(options)
    @from = options[:from_file]
    @to = options[:to_file]
    @bucket = options[:bucket]
    @region = options[:region]
    @akid = options[:akid]
    @secret_key = options[:secret_key]
  end

  def download()
    s3 = Aws::S3::Client.new(
      credentials: Aws::Credentials.new(@akid, @secret_key),
      region: @region
    )

    File.open(@to, 'wb') do |file|
      reap = s3.get_object({ bucket:@bucket, key:@from }, target: file)
      file.close()
    end

    print "Successfully downloaded to this path: " + @to + "/n"
  end
end