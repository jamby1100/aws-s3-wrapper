require 'optparse'
require_relative 's3_uploader'
require_relative 's3_downloader'
require_relative 's3_lister'


def parse_args
  options = {}
  OptionParser.new do |opt|
    opt.on('--mode MODE') { |o| options[:mode] = o }
    opt.on('--from_file FROM') { |o| options[:from_file] = o }
    opt.on('--bucket BUCKET') { |o| options[:bucket] = o }
    opt.on('--region REGION') { |o| options[:region] = o }
    opt.on('--credentials CREDENTIALS') { |o| options[:credentials] = o }
    opt.on('--to_file TO') { |o| options[:to_file] = o }
  end.parse!

  options
end

def check_args
  # parse arguments and initialize params
  options = parse_args()
  error_msg = ""

  # verify if a param was submitted
  if options.has_key?(:credentials)
    # open and parse credential file
    f = open(options[:credentials], "r")
    credential_string = f.read()
    credentials = credential_string.chomp.split(",")

    # populate credentials
    options[:akid] = credentials[0]
    options[:secret_key] = credentials[1]
  else
    raise "[Error: NoCredentialError] This script requires a credentials file containg APID and Secret Key"
  end

  # region details
  unless options.has_key?(:region)
    options[:region] = "ap-southeast-1"
  end

  # bucket details
  unless options.has_key?(:bucket)
    options[:bucket] = "jamby-test-bucket-12345"
  end

  if ["upload","download"].include?(options[:mode])
    unless options.has_key?(:from_file) or options.has_key?(:to_file)
      raise "[Error: NoFromFile/NoToFile] To and From Files are required for upload/download modes"
    end
  elsif options[:mode] != "list"
    raise "[Error: ModeDoesNotExist] Script only accepts upload,download,list as modes"
  end

  return options
end

def implement(options)
  options = check_args() if options.empty?
  
  if options[:mode] == "upload"
    uploader = Uploader.new(options)
    uploader.upload()
  elsif options[:mode] == "download"
    downloader = Downloader.new(options)
    downloader.download()
  elsif options[:mode] == "list"
    lister = Lister.new(options)
    lister.list()    
  end
end

implement()