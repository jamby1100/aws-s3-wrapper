require 'aws-sdk'
class Lister
  def initialize(options)
    @bucket = options[:bucket]
    @region = options[:region]
    @akid = options[:akid]
    @secret_key = options[:secret_key]
  end

  def list()
    s3 = Aws::S3::Resource.new(
      credentials: Aws::Credentials.new(@akid, @secret_key),
      region: @region
    )

    bucket = s3.bucket(@bucket)
    bucket.objects.each do |obj|
      puts "#{obj.key} => #{obj.public_url.to_s}"
    end
  end
end